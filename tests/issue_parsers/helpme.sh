#!/usr/bin/env bash

file=$1
echo $file

sed -i 's/softly\.assertThat.*$/-/g' $file
sed -i 's/\.hasLineStart(/line_start: /g' $file
sed -i 's/\.hasLineEnd(/line_end: /g' $file
sed -i 's/\.hasColumnStart(/column_start:\ /g' $file
sed -i 's/\.hasColumnEnd(/column_end:\ /g' $file
sed -i 's/\.hasFileName(/filename:\ /g' $file
sed -i 's/\.hasMessage(/message:\ /g' $file
sed -i 's/\.hasType(/type:\ /g' $file
sed -i 's/\.hasCategory(/category:\ /g' $file
sed -i 's/\.hasSeverity(/severity:\ /g' $file
sed -i 's/);$//g' $file
sed -i 's/)$//g' $file
sed -i 's/Severity\.//g' $file
