from pathlib import Path

import pytest

from pysaps.issue.parsers import *  # noqa: F403
from pysaps.issue.serializers import *  # noqa: F403

this_dir = Path(__file__).parent


@pytest.mark.parametrize('klass', IssueParser.__subclasses__())  # noqa: F405
@pytest.mark.parametrize('serializer', Serializer.__subclasses__())  # noqa: F405
@pytest.mark.demo
def test_gitlab_serializer_produces_gitlab_reports(klass, serializer):
    content = open(this_dir / f'{klass.__name__}.txt').read()
    issues = klass().parse(content)
    filename = Path('code_quality.json')
    serializer().serialize_to_file(issues, filename, append=True)
    assert filename.is_file()
