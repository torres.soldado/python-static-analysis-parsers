import argparse
import json
from pathlib import Path
from sys import stdin
from sys import stdout

from pysaps.issue.parsers import *  # noqa: F403
from pysaps.issue.serializers import Serializer  # noqa: F403


def main():
    parsers = IssueParser.__subclasses__()  # noqa: F405
    serializers = Serializer.__subclasses__()  # noqa: F405
    parser = argparse.ArgumentParser(description='Python Static Analysis Parsers.')
    parser.add_argument('--parser', choices=list(k.__name__ for k in parsers),
                        help='Specify a parser. If unspecified parser with most results will be used.')
    parser.add_argument('--input,-i', dest='input', type=Path,
                        help='Specify input file to parse. If unspecified, input will be read from stdin')
    parser.add_argument('--output,-o', dest='output', type=Path,
                        help='Specify output file to parse. If unspecified, output will be written to stdout')
    parser.add_argument('--output-append', action='store_true', help='Append to exist output file, if it exists')
    parser.add_argument('--serializer', choices=list(k.__name__ for k in serializers), help='Serialization format')
    args = parser.parse_args()

    if args.parser is not None:
        parsers = [p for p in parsers if p.__name__ == args.parser]
    assert len(parsers) > 0

    serializer = Serializer  # noqa: F405
    if args.serializer is not None:
        serializer = [s for s in serializers if s.__name__ == args.serializer][0]

    if args.input is not None:
        assert args.input.is_file(), f"Couldn't find file \"{args.input}\""
        content = open(args.input).read()
    else:
        content = '\n'.join([line for line in stdin])

    issues = []
    for parser in parsers:
        _issues = parser().parse(content)
        if len(_issues) > len(issues):
            issues = _issues

    issues_serialized = serializer().serialize(issues)
    output = stdout
    if args.output is not None:
        if args.output_append and args.output.is_file():
            with open('args.output', 'r') as _f:
                _issues = json.load(_f)
                issues_serialized.extend(_issues)
        output = open(args.output, 'w')
    json.dump(issues_serialized, output)


if __name__ == "__main__":
    main()
